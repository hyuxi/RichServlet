
package com.apache.rich.servlet.http.servlet.realize.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpSessionStoreImpl implements
        RichServletHttpSessionStore {

    private static final Logger log = LoggerFactory.getLogger(RichServletHttpSessionStoreImpl.class);

    public static ConcurrentHashMap<String, RichServletHttpSession> sessions = new ConcurrentHashMap<String, RichServletHttpSession>();

    @Override
    public RichServletHttpSession createSession() {
        String sessionId = this.generateNewSessionId();
        log.debug("Creating new session with id {}", sessionId);

        RichServletHttpSession session = new RichServletHttpSession(sessionId);
        return session;
    }

    @Override
    public void destroySession(String sessionId) {
        log.debug("Destroying session with id {}", sessionId);
        sessions.remove(sessionId);
    }

    @Override
    public RichServletHttpSession findSession(String sessionId) {
        if (sessionId == null)
            return null;

        return sessions.get(sessionId);
    }

    protected String generateNewSessionId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void destroyInactiveSessions() {
        for (Map.Entry<String, RichServletHttpSession> entry : sessions.entrySet()) {
        		RichServletHttpSession session = entry.getValue();
            if (session.getMaxInactiveInterval() < 0)
                continue;

            long currentMillis = System.currentTimeMillis();

            if (currentMillis - session.getLastAccessedTime() > session
                    .getMaxInactiveInterval() * 1000) {

                destroySession(entry.getKey());
            }
        }
    }

}
