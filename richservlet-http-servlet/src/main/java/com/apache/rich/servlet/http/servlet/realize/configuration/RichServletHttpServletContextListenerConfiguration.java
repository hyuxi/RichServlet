
package com.apache.rich.servlet.http.servlet.realize.configuration;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletContext;

import com.apache.rich.servlet.http.servlet.realize.utils.HttpServletUtils;

/**
 * 上下文监听配置类
 * @author wanghailing
 *
 */
public class RichServletHttpServletContextListenerConfiguration {

    private static final Logger log = LoggerFactory.getLogger(RichServletHttpServletContextListenerConfiguration.class);

    private ServletContextListener listener;

    private boolean initialized = false;

    public RichServletHttpServletContextListenerConfiguration(
            Class<? extends ServletContextListener> clazz) {
        this(HttpServletUtils.newInstance(clazz));
    }

    public RichServletHttpServletContextListenerConfiguration(ServletContextListener listener) {
        this.listener = listener;
    }

    public ServletContextListener getListener() {
        return listener;
    }

    public void init() {
        try {

            log.debug("Initializing listener: {}", this.listener.getClass());

            this.listener.contextInitialized(new ServletContextEvent(
            		RichServletHttpServletContext.get()));
            this.initialized = true;

        } catch (Exception e) {

            this.initialized = false;
            log.error("Listener '" + this.listener.getClass()
                    + "' was not initialized!", e);
        }
    }

    public void destroy() {
        try {

            log.debug("Destroying listener: {}", this.listener.getClass());

            this.listener.contextDestroyed(new ServletContextEvent(
            		RichServletHttpServletContext.get()));
            this.initialized = false;

        } catch (Exception e) {

            this.initialized = false;
            log.error("Listener '" + this.listener.getClass()
                    + "' was not destroyed!", e);
        }
    }

    public boolean isInitialized() {
        return initialized;
    }
}
