package com.apache.rich.servlet.core.server.utils;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;

import io.netty.handler.codec.http.FullHttpRequest;

/**
 * http 工具类
 * @author wanghailing
 *
 */
public class HttpUtils {
	/**
	 * 转移成http请求
	 * @param request
	 * @return
	 */
    public static RequestMethodEnums convertHttpMethodFromNetty(FullHttpRequest request) {
        try {
            return RequestMethodEnums.valueOf(request.getMethod().name().toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return RequestMethodEnums.UNKOWN;
        }
    }

    public static String truncateUrl(String url) {
        if (url.contains("?"))
            url = url.substring(0, url.indexOf("?"));
        return url;
    }
}
