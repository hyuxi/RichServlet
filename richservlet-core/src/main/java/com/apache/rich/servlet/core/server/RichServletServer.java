package com.apache.rich.servlet.core.server;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.apache.rich.servlet.core.server.rest.ControllerRouter;
import com.apache.rich.servlet.core.server.rest.HttpURLResource;

import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
import com.apache.rich.servlet.core.server.rest.interceptor.Interceptor;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider;
import com.apache.rich.servlet.core.server.rest.controller.DefaultController;
import com.apache.rich.servlet.core.server.rest.controller.URLController;
import com.apache.rich.servlet.core.annotation.Controller;
import com.apache.rich.servlet.core.annotation.RequestMapping;
import com.apache.rich.servlet.common.utils.ClassUtils;
import com.apache.rich.servlet.common.utils.PackageScannerUtils;
import com.apache.rich.servlet.common.exception.HServerException;
import com.apache.rich.servlet.common.enums.ErrorCodeEnums;
import com.apache.rich.servlet.common.enums.RequestMethodEnums;

/**
 * scan package route service provider
 * <p>
 * Author wanghailing
 */
public abstract class RichServletServer extends RichServletServerOptionProvider implements Server {

	// controller router map collection
	private final ControllerRouter routerTable = new ControllerRouter();
	// interceptors collection
	private final List<Interceptor> interceptors = new LinkedList<>();
	// only once initail scan
	private final AtomicBoolean scanOver = new AtomicBoolean(false);
	// root controller
	private static boolean hasRootController = true;

	// scan specified package's all classes
	private PackageScannerUtils scanner = new PackageScannerUtils();

	public void disableInternalController() {
		hasRootController = false;
	}

	/**
	 * we install internal module here, just one time !
	 */
	void firstScan() {
		if (scanOver.compareAndSet(false, true)) {
			if (hasRootController) {
				// default Controller (URI path is "/")
				Method root = DefaultController.class.getMethods()[0];
				routerTable.register(
						HttpURLResource.fromHttp("/", RequestMethodEnums.GET),
						URLController.fromProvider("/",
								DefaultController.class, root).internal());
			}

			// // internal Interceptor
			// for (InterceptorHelpers interceptor :
			// InterceptorHelpers.values()) {
			// interceptor.instance.install(this);
			// interceptors.add(interceptor.instance);
			// }
		}
	}

	// scan package controller class. NOT Threads-Safe !!
	//
	public RichServletServer scanHttpController(String packageName) throws HServerException {
		firstScan();

		// scan all classes and install them
		List<Class<?>> classList = null;
		try {
			classList = scanner.scan(packageName);
		} catch (ClassNotFoundException e) {
			throw new HServerException(
					ErrorCodeEnums.SCAN_PACKGE_ERROR.getErrorCode(), String.format(
							"scan package %s failed. %s", packageName,
							e.getMessage()));
		}

		RequestMapping clazzLevelRequestMapping = null;

		for (Class<?> clazz : classList) {
			// @Interceptor
			if (clazz
					.getAnnotation(com.apache.rich.servlet.core.annotation.Interceptor.class) != null) {
				checkConstructor(clazz);
				// must implements Interceptor
				if (clazz.getSuperclass() != Interceptor.class) {
					throw new HServerException(
							ErrorCodeEnums.SYSTEM_ERROR.getErrorCode(),
							String.format("%s must implements %s",
									clazz.getName(),
									Interceptor.class.getName()));
				}
				try {

					/**
					 * we register interceptors with class-name natural
					 * sequence. may be known as unordered. But make ensurance
					 * that call with registered sequence.
					 *
					 * TODO : may be we can indecate the previous
					 * {@link org.nesty.commons.annotations.Interceptor} manully
					 *
					 */
					interceptors.add((Interceptor) clazz.newInstance());
				} catch (InstantiationException | IllegalAccessException e) {
					throw new HServerException(
							ErrorCodeEnums.SYSTEM_ERROR.getErrorCode(),
							String.format("%s newInstance() failed %s",
									clazz.getName(), e.getMessage()));
				}
			}

			// with @Controller
			if (clazz.getAnnotation(Controller.class) != null) {
				checkConstructor(clazz);
				// class level prefix RequestMapping.URL
				clazzLevelRequestMapping = clazz
						.getAnnotation(RequestMapping.class);

				// find all annotationed method in class
				Method[] methods = clazz.getMethods();
				for (Method method : methods) {
					if (Modifier.isStatic(method.getModifiers())
							|| method.getAnnotations().length == 0) {
						continue;
					}
					// read @RequestMapping
					RequestMapping requestMapping = method
							.getAnnotation(RequestMapping.class);
					if (requestMapping == null) {
						continue;
					}
					String uri = requestMapping.value();
					if (!uri.startsWith("/")) {
						throw new HServerException(
								ErrorCodeEnums.SYSTEM_ERROR.getErrorCode(),
								String.format(
										"%s.%s annotation must start with / ",
										clazz.getName(), method.getName()));
					}
					if (clazzLevelRequestMapping != null) {
						uri = clazzLevelRequestMapping.value() + uri;
					}
					// default is RequestMethod.GET if method annotation is not
					// set
					RequestMethodEnums requestMethod = requestMapping.method();

					HttpURLResource urlResource = HttpURLResource.fromHttp(uri,
							requestMethod);
					URLController urlController = URLController.fromProvider(
							uri, clazz, method);

					/**
					 * register the controller to controller map
					 * {@link ControllerRouter}.register() will return false on
					 * dupliacted URLReousource key. Duplicated URLResource
					 * means they have same url, url variabls and http method.
					 * we will confuse on them and couldn't decide which
					 * controller method to invoke.
					 *
					 */
					if (!routerTable.register(urlResource, urlController)) {
						throw new HServerException(
								ErrorCodeEnums.SYSTEM_ERROR.getErrorCode(),
								String.format("%s.%s annotation is duplicated",
										clazz.getName(), method.getName()));
					}
					// add monitor
					RichServletServerMonitor.resourceMap.put(urlResource, urlController);
				}
			}
		}
		return this;
	}

	private boolean checkConstructor(Class<?> clazz) throws HServerException {
		if (!ClassUtils.hasDefaultConstructor(clazz)) {
			throw new HServerException(
					ErrorCodeEnums.SYSTEM_ERROR.getErrorCode(), String.format(
							"%s dosn't have default constructor",
							clazz.getName()));
		}
		return true;
	}

	public ControllerRouter getRouteController() {
		return routerTable;
	}

	public List<Interceptor> getInterceptor() {
		return interceptors;
	}
}