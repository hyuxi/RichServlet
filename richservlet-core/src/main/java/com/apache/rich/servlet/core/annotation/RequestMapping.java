package com.apache.rich.servlet.core.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;

import com.apache.rich.servlet.common.constants.HttpConstants;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMapping {
	
    String value();
    
    RequestMethodEnums method() default RequestMethodEnums.GET;
    
    String httpTypeJson() default HttpConstants.HEADER_CONTENT_TYPE_JSON;
    
    String httpTypeText() default HttpConstants.HEADER_CONTENT_TYPE_TEXT;
}