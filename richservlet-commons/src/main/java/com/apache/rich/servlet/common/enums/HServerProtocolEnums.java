package com.apache.rich.servlet.common.enums;

public enum HServerProtocolEnums {
	HTTP, HTTPS, SPDY, HTTP2;
}