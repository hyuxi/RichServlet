package com.apache.rich.servlet.common.httpclient.impl;

import com.apache.rich.servlet.common.httpclient.HttpRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Random;
import java.util.Map.Entry;

public class UrlMultipartEntity {
	private static final char[] CLRF = new char[]{'\r', '\n'};
	private static final char[] DOUBLE_CLRF = new char[]{'\r', '\n', '\r', '\n'};
	private static final String BOUNDARY_START = "---------------------------HttpAPIFormBoundary";

	public void writeDataToBody(HttpURLConnection connection,
			HttpRequest request) throws Exception {
		String boundary = "---------------------------HttpAPIFormBoundary"
				+ (new Random()).nextLong();
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type",
				"multipart/form-data; boundary=" + boundary);
		connection.setRequestMethod("POST");
		connection.setUseCaches(false);
		boundary = "--" + boundary;
		OutputStream os = connection.getOutputStream();
		OutputStreamWriter writer = null;

		try {
			writer = new OutputStreamWriter(os);
			Iterator e = request.getParamMap().entrySet().iterator();

			Entry entry;
			while (e.hasNext()) {
				entry = (Entry) e.next();
				writer.write(boundary);
				writer.write(CLRF);
				writer.write("Content-Disposition: form-data; name=\""
						+ (String) entry.getKey() + "\"");
				writer.write(DOUBLE_CLRF);
				writer.write((String) entry.getValue());
				writer.write(CLRF);
			}

			for (e = request.getFileParamMap().entrySet().iterator(); e
					.hasNext(); writer.write(CLRF)) {
				entry = (Entry) e.next();
				writer.write(boundary);
				writer.write(CLRF);
				writer.write("Content-Disposition: form-data; name=\""
						+ (String) entry.getKey() + "\"");
				writer.write("; filename=\""
						+ ((File) entry.getValue()).getName() + "\"");
				writer.write(CLRF);
				String type = URLConnection
						.guessContentTypeFromName(((File) entry.getValue())
								.getName());
				if (type == null) {
					type = "application/octet-stream";
				}

				writer.write("Content-Type: ");
				writer.write(type);
				writer.write(DOUBLE_CLRF);
				writer.flush();
				FileInputStream input = null;

				try {
					input = new FileInputStream((File) entry.getValue());
					byte[] e1 = new byte[1024];

					while (true) {
						int read = input.read(e1, 0, e1.length);
						if (read == -1) {
							os.flush();
							break;
						}

						os.write(e1, 0, read);
					}
				} catch (Exception arg32) {
					throw new Exception(arg32);
				} finally {
					try {
						input.close();
					} catch (Exception arg31) {
						;
					}

				}
			}

			boundary = boundary + "--";
			writer.write(boundary);
			writer.write(CLRF);
		} catch (Exception arg34) {
			throw new Exception(arg34);
		} finally {
			try {
				writer.close();
			} catch (Exception arg30) {
				;
			}

		}
	}
}