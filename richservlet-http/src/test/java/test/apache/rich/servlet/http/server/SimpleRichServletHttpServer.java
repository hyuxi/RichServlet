///**
// * 
// */
//package test.apache.rich.servlet.http.server;
//
//import com.apache.rich.servlet.http.server.RichServletHttpServer;
//import com.apache.rich.servlet.http.server.RichServletHttpServerProvider;
//
//import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
//import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
//
///**
// * @author wanghailing
// *
// */
//public class SimpleRichServletHttpServer {
//
//	/**
//	 * @param args
//	 * @throws Exception 
//	 */
//	public static void main(String[] args) throws Exception {
//		RichServletHttpServer richServletHttpServer=RichServletHttpServerProvider.getInstance().service();
//		// disable internal root path stats controller
//		richServletHttpServer.disableInternalController();
//        // disable stats monitor
//        RichServletServerMonitor.disable();
//
//        // 2. choose http params. this is unnecessary
//        richServletHttpServer.option(RichServletServerOptions.IO_THREADS, Runtime.getRuntime().availableProcessors())
//                .option(RichServletServerOptions.WORKER_THREADS, 128)
//                .option(RichServletServerOptions.TCP_BACKLOG, 1024)
//                .option(RichServletServerOptions.TCP_NODELAY, true)
//                .option(RichServletServerOptions.TCP_TIMEOUT, 10000L)
//                .option(RichServletServerOptions.TCP_ADRESS,"127.0.0.1")
//                .option(RichServletServerOptions.TCP_PORT,8080)
//                .option(RichServletServerOptions.MAX_CONNETIONS, 4096);
//
//        richServletHttpServer.scanHttpController("test.apache.rich.servlet.http.server.rest");
//
//        // 3. start http server
//        if (!richServletHttpServer.start()){
//            System.err.println("HttpServer run failed");
//        } 
//        try {
//            // join and wait here
//        		richServletHttpServer.join();
//        		richServletHttpServer.shutdown();
//        } catch (InterruptedException ignored) {
//        }
//	}
//
//}
