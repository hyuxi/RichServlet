package test.apache.rich.servlet.http.server.rest;


import java.util.List;

import com.apache.rich.servlet.core.annotation.PathVariable;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;
import com.apache.rich.servlet.core.annotation.ResponseBody;
import com.apache.rich.servlet.core.annotation.RequestParam;
import com.apache.rich.servlet.core.annotation.RequestMapping;
import com.apache.rich.servlet.core.annotation.Controller;

@Controller
public class Controller1 {

    @RequestMapping(value = "/contract/new", method = RequestMethodEnums.POST)
    public
    @ResponseBody
    HttpResult<Boolean> createNewContract(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("baseId") String baseId,
            @RequestParam("resourceType") Integer resourceType,
            @RequestParam("specs") String specs
    ) {
        return new HttpResult<Boolean>();
    }

    @RequestMapping(value = "/contract/upgrade", method = RequestMethodEnums.POST)
    public
    @ResponseBody
    HttpResult<Boolean> createUpgradeContract(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("baseId") String baseId,
            @RequestParam("resourceId") Long resourceId,
            @RequestParam("specs") String specs) {
        return new HttpResult<Boolean>();
    }

    @RequestMapping(value = "/contract/renew", method = RequestMethodEnums.POST)
    public
    @ResponseBody
    HttpResult<Boolean> createRenewContract(
            @RequestParam("tenantId") Long tenantId,
            @RequestParam("baseId") String baseId,
            @RequestParam("resourceId") Long resourceId,
            @RequestParam("specs") String specs) {

        return new HttpResult<Boolean>();
    }

    @RequestMapping(value = "/contract/{tenantId}/{contractId}", method = RequestMethodEnums.DELETE)
    public
    @ResponseBody
    HttpResult<String> cancelContract(
            @PathVariable(value = "contractId") Long contractId,
            @PathVariable(value = "tenantId") Long tenantId,
            @RequestParam(value = "baseId") String baseId
    ) {
        return new HttpResult<String>();
    }

    @RequestMapping(value = "/contract/{tenantId}/{contractId}", method = RequestMethodEnums.GET)
    public
    @ResponseBody
    HttpResult<Boolean> getContract(
            @PathVariable(value = "contractId") Long contractId,
            @PathVariable(value = "tenantId") Long tenantId
    ) {
        return new HttpResult<Boolean>();
    }

    @RequestMapping(value = "/contract/{tenantId}", method = RequestMethodEnums.GET)
    public
    @ResponseBody HttpResult<Boolean> listContract(
            @PathVariable(value = "tenantId") Long tenantId,
            @RequestParam(value = "offset") Long offset,
            @RequestParam(value = "pageSize") Integer pageSize,
            @RequestParam(value = "orderStatus", required = false) Integer orderStatus
    ) {

        return new HttpResult<Boolean>();
    }

    
    @RequestMapping(value = "/contractUnpay/{tenantId}", method = RequestMethodEnums.GET)
    public
    @ResponseBody HttpResult<List<Boolean>> listUnpayContract(
            @PathVariable(value = "tenantId") Long tenantId
    ) {
        return new HttpResult<List<Boolean>>();
    }

    @RequestMapping(value = "/contractCount/{tenantId}", method = RequestMethodEnums.GET)
    public @ResponseBody
    HttpResult<List<Boolean>> getContractCount(
            @PathVariable(value = "tenantId") Long tenantId
    ) {
        return new HttpResult<List<Boolean>>();
    }
}

